# Vue Use Form

## Fields

### example

html

    <template>
        <input ref="refInput" />
    <template>

js

    import { useField, useFieldGroup } from "@gurso/vue-use-form"

    const refInput = ref()
    const { dirty, touched, valid, blank, model } = useField({ field: refInput })
    // there also pristine, invalid, untouched

respectives CSS class are added to the element.

You can pass default value in argument:

    const { dirty, touched, valid, blank, model } = useField({ field: refInput, value: "foo" })

if you use v-model on element, you should pass to:

html

    <template>
        <input ref="refInput" v-model="foo" />
    <template>

js

    const model = ref("foo")
    const refInput = ref()
    const { dirty, touched, valid, blank } = useField({ field: refInput, model })
