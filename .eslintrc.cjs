// eslint-disable-next-line no-undef
module.exports = {
	root: true,
	parser: "vue-eslint-parser",
	parserOptions: {
		parser: "@typescript-eslint/parser",
		sourceType: "module",
	},
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/eslint-recommended",
		"plugin:@typescript-eslint/recommended",
		"@gurso",
		"plugin:vue/vue3-essential",
	],
	rules: {
		"no-unused-vars": "off",
		"@typescript-eslint/no-unused-vars": "warn",
	},
}
