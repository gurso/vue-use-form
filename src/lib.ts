import { ref, watch, computed, Ref } from "vue"

type Rule = (v?: unknown) => boolean
type FieldElement = HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement | HTMLButtonElement
type FieldGroupElement = HTMLFormElement | HTMLFieldSetElement

function useReverse(b: Ref<boolean | undefined>) {
	return computed({
		get() {
			return !b.value
		},
		set(v: boolean) {
			b.value = !v
		},
	})
}

function useIsDirty(refValue: Ref<unknown>) {
	const dirty = ref(false)
	watch(refValue, () => {
		dirty.value = true
	})
	return dirty
}

function useIsValid(refValue: Ref<unknown>, ...rules: Rule[]) {
	const valid = ref(true)
	watch(
		refValue,
		() => {
			valid.value = rules.every(r => r(refValue.value))
		},
		{ immediate: true },
	)
	return valid
}

function useIsFieldValid(field: Ref<FieldElement | undefined>) {
	const valid = ref<boolean>()
	function isValid() {
		valid.value = field.value?.validity.valid
	}
	isValid()
	watch(
		field,
		el => {
			el?.addEventListener("input", isValid)
			el?.addEventListener("change", isValid)
		},
		{ immediate: true },
	)
	return valid
}

function useGetValue(field: Ref<FieldElement | undefined>) {
	const model = ref<string | number | boolean | undefined>(field.value && getValue(field.value))
	function update(e: Event) {
		model!.value = getValue(e.target as FieldElement)
	}
	watch(
		field,
		el => {
			if (el) {
				el.addEventListener("input", update)
				el.addEventListener("change", update)
			}
		},
		{ immediate: true },
	)
	return model
}

function getValue(el: FieldElement) {
	return "valueAsDate" in el && el.valueAsDate
		? el.value // ? el.valueAsDate
		: "valueAsNumber" in el && Number.isFinite(el.valueAsNumber)
			? el.valueAsNumber
			: el.type === "checkbox" && "checked" in el
				? el.checked
				: el.value
}

function useFieldInfos(field: Ref<FieldElement | undefined>) {
	const touched = ref(false)
	const focus = ref(false)

	watch(
		field,
		el => {
			el?.addEventListener("focus", () => {
				touched.value = focus.value = true
			})
			el?.addEventListener("blur", () => {
				focus.value = false
			})
		},
		{ immediate: true },
	)
	return { touched, focus }
}

function useSetClasses(name: string, el: Ref<HTMLElement | undefined>, meta: Ref<boolean | undefined>) {
	watch(
		meta,
		v => {
			if (v) el.value?.classList.add(name)
			else el.value?.classList.remove(name)
		},
		{ immediate: true },
	)
}

const rulesByElement = new WeakMap<Element, Rule[]>()
function useSaveRules(field: Ref<FieldElement | undefined>, rules: Rule[]) {
	watch(field, (el, previous) => {
		if (el) rulesByElement.set(el, rules)
		else if (previous) rulesByElement.delete(previous)
	})
}

export function useMeta(model: Ref<unknown>, ...rules: Rule[]) {
	const refModel = ref(model)
	const dirty = useIsDirty(refModel)
	const pristine = useReverse(dirty)
	const valid = useIsValid(refModel, ...rules)
	const invalid = useReverse(valid)
	const blank = computed(() => !refModel.value)
	return { dirty, pristine, valid, invalid, blank }
}

type FieldOptions = {
	field: Ref<FieldElement | undefined>
	model?: Ref<unknown>
	rules?: Rule[]
	// isNumber
	// isDate
}
export function useField({ field, model, rules = [] }: FieldOptions) {
	const refModel = model ?? useGetValue(field)
	const { valid: modelValid, pristine, dirty, blank } = useMeta(refModel, ...rules)

	const fieldValid = useIsFieldValid(field)
	const { touched, focus } = useFieldInfos(field)

	const valid = computed(() => modelValid.value && fieldValid.value)
	const invalid = computed(() => !valid.value)

	useSaveRules(field, rules)

	useSetClasses("dirty", field, dirty)
	useSetClasses("pristine", field, pristine)
	useSetClasses("valid", field, valid)
	useSetClasses("invalid", field, invalid)
	useSetClasses("blank", field, blank)
	useSetClasses("touched", field, touched)
	useSetClasses("focus", field, focus)

	return { dirty, pristine, valid, invalid, touched, focus, blank }
}

export function useFieldGroup(field: Ref<FieldGroupElement | undefined>) {
	const valid = ref<boolean>()
	const dirty = ref(false)
	const invalid = useReverse(valid)
	const pristine = useReverse(dirty)

	useSetClasses("dirty", field, dirty)
	useSetClasses("pristine", field, pristine)
	useSetClasses("valid", field, valid)
	useSetClasses("invalid", field, invalid)

	const onChange = () => {
		const childrenFilter = (child: Element): child is FieldElement => rulesByElement.has(child)
		const children = Array.from(field.value?.elements ?? []).filter(childrenFilter)
		dirty.value = true
		valid.value =
			field.value?.checkValidity() &&
			children.every(child => rulesByElement.get(child)?.every(rule => rule(getValue(child))))
	}
	watch(
		field,
		el => {
			el?.addEventListener("change", onChange)
			el?.addEventListener("input", onChange)
		},
		{ immediate: true },
	)
	return { valid, dirty, invalid, pristine }
}
