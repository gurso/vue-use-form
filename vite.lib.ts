import path from "path"
import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"

export default defineConfig({
	plugins: [vue()],
	build: {
		minify: true,
		lib: {
			// eslint-disable-next-line no-undef
			entry: path.resolve(__dirname, "src/lib.ts"),
			fileName: "index",
			name: "vue-use-form",
			formats: ["es"],
		},
		rollupOptions: {
			// make sure to externalize deps that shouldn't be bundled
			// into your library
			external: ["vue"],
			output: {
				// Provide global variables to use in the UMD build
				// for externalized deps
				globals: {
					vue: "Vue",
				},
			},
		},
	},
})
